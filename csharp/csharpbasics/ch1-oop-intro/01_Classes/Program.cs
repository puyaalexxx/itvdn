﻿using System;

namespace _01_Classes
{
    class MyClass
    {
        public string field;
        public void Method()
        {
            Console.WriteLine(field);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //weak link
            new MyClass();

            //create instance
            var instance = new MyClass();

            //adding value to field field
            instance.field = "Hello";

            Console.WriteLine(instance.field);

            //method call
            instance.Method();
        }
    }
}
