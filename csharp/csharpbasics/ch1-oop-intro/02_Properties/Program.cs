﻿using System;

namespace _02_Properties
{
    class MyClass {
        private string field = null;

        public void SetField(string value) //setter
        {
            field = value;
        }

        public string GetField()
        {
            return field;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var instance = new MyClass();

            instance.SetField("Hello World");

            string @string = instance.GetField();

            Console.WriteLine(@string);
        }
    }
}
