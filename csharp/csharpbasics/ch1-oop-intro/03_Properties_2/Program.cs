﻿using System;

namespace _03_Properties_2
{

    public class MyClass
    {
        private string field = null;

        public string Field
        {
            set
            {
                field = value;
            }

            get
            {
                return field;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var instance = new MyClass();

            instance.Field = "Hello World";

            string @string = instance.Field;

            Console.WriteLine(@string);
        }
    }
}
