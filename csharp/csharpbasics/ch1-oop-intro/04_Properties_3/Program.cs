﻿using System;

namespace _04_Properties_3
{
    public class MyClass
    {
        string field = null;

        public string Field
        {
            set
            {
                if (value == "Goodbye")
                {
                    Console.WriteLine("Not allowed");
                }
                else
                {
                    field = value;
                }
            }

            get
            {
                if (field == null)
                {
                    return "No value defined";
                }

                return field;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var instance = new MyClass();

            instance.Field = "Goodbye";
            Console.WriteLine(instance.Field);


            instance.Field = "Hello World";

            string @string = instance.Field;

            Console.WriteLine(@string);
        }
    }
}
