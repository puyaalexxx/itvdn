﻿using System;

namespace _05_Properties_04
{
    class Constants
    {
        private double pi = 3.14;
        private double e = 2.71;

        //writeonly property
        public double Pi
        {
            set
            {
                pi = value;
            }
        }

        //readonly property
        public double E
        {
            get { return e; }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            
        }
    }
}
