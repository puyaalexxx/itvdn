﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_Constructors
{
    class Point
    {
        private int x, y;

        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public Point()
        {
            Console.WriteLine("Parametrless Contructor");
        }

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
