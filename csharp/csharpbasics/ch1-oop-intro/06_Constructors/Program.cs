﻿using System;

namespace _06_Constructors
{
    class Program
    {
        static void Main(string[] args)
        {
            var pointA = new Point();
            Console.WriteLine($"Parametrless constructor x = { pointA.X }; y = {pointA.Y}");

            var pointB = new Point(100, 200);
            Console.WriteLine($"Arguments constructor x = { pointB.X }; y = {pointB.Y}");
        }
    }
}
