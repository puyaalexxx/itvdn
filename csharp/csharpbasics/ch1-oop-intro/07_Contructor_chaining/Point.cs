﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_Contructor_chaining
{
    class Point
    {
        private int x, y;

        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public Point(int x) : this(x, 300)
        {
            this.x = x;
        }

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }


    }
}
