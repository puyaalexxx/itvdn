﻿using System;

namespace _07_Contructor_chaining
{
    class Program
    {
        static void Main(string[] args)
        {
            Point pointB = new Point(100, 200);

            Console.WriteLine($"x = {pointB.X}; y = {pointB.Y}");
        }
    }
}
