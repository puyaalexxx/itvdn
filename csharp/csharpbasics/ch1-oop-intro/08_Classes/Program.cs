﻿using System;

namespace _08_Classes
{
    class Program
    {
        class MyClass
        {
            public void Method()
            {
                Console.WriteLine("Method from class MyClass");
            }
        }

        class MyClass2
        {
            public void CallMethod(MyClass my)
            {
                my.Method();
            }
        }

        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            MyClass2 my2 = new MyClass2();

            my2.CallMethod(my);
        }
    }
}
