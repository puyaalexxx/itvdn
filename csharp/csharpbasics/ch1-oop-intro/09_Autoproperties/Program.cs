﻿using System;

namespace _09_Autoproperties
{
    class Author
    {
        public string Name { get; set; }

        public string Book { get; set; }

        //readonly propery
        public int Age { get; } = 30;

        public int Note { get; private set; } = 30;

        //init propery, only in object initialization or constructors
        public int Number { get; init; }

        public int MyProperty { private get; set; }

        public Author()
        {

        }
        
        public Author(int note, int age)
        {
            this.Note = note;
            this.Age = age;
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Author author = new Author();

            author.MyProperty = 20;
            //Console.WriteLine(author.MyProperty);

            //private set
            //author.Note = 20;
        }
    }
}
